﻿using Foundation.ModConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vsprospectorinfo.src
{
    public class ModConfig : ModConfigBase
    {
        public override string ModCode
        {
            get
            {
                return "vsprospectorinfo";
            }
        }

        public bool RenderTexturesOnMap { get; set; } = true;
    }
}
