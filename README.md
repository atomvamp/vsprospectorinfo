# VsProspectorInfo
A clientside only mod to save trees by not having to write down the values of your prospecting.
## Installation
Just drop the file into your mods folder. You just need this mod on the client, no serverside installation needed.

## Usage
After prospecting, the info will be displayed in the tooltip of the minimap when hovering over the chunk. This info is stored in %Vintage_Story_Data%/Mods/Data/YourWorldSeed/PospectorInfo.prospectorMessages.json and is client side only.

![image](https://user-images.githubusercontent.com/5238284/79952656-09e3f680-847b-11ea-96c9-b4cb9b47355f.png)

This version is a forked version of the original ProspectorInfo mod. It works with v1.14.2 and has a few other changes:

- fixes some bugs
- renders a texture on the map that shows which chunks you have actually prospected. This can be toggled with the `.pi` command. It looks as shown in the following picture.
- it updates the chunk information every time you prospect, instead of having to reconnect or reload

![image](https://i.imgur.com/RBAGBBg.png)
